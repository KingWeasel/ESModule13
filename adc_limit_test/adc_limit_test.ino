#define PWM_PIN 7
#define ADC_PIN_0 15
#define NUM_ADC 6
#define MAX_DUTY_CYCLE 65535
#define MAX_ADC_VAL 4095
#define NUM_SAMPLES 100
#define ADC_LSB2VOLT 0.000805664//Volts per bit


//text input//////////////////////////////////
#define STRING_MAX 200

String inputString = "";// a String to hold incoming data
////////////////////////////////////////////////////////////////

void setup()
{
  // initialize serial
  Serial.begin(115200);
  while (!Serial);
  Serial.flush();
  Serial.println("Starting...");
  Serial.println();

  // reserve STRING_MAX bytes for the inputString
  inputString.reserve(STRING_MAX);
}

void loop()
{
  while (Serial.available() > 0)
    inputString.concat((char)Serial.read());

  if (inputString.length() > 0)
  {
    process_string(inputString);
    inputString.remove(0);
  }
}

void process_string(String inputString)
{
  String int_holder = "";
  int_holder.reserve(STRING_MAX);

  do_adc_test();
}

void print_help()
{
  Serial.println("Press any key to start/repeat test...\n");
}

void do_adc_test()
{
  double readings[NUM_SAMPLES][NUM_ADC];//"table" that is sent to host computer

  Serial.println("Beginning ADCtest...");
  Serial.println("----------------");
  Serial.println();

  for (;;)
  {
    for (int i = 0; i < ( sizeof(readings[i]) / sizeof(readings[0][0]) ); i++)
    {
      for (int j = 0; j < NUM_ADC; j++)
      {
        readings[i][j] = analogRead(ADC_PIN_0 + j) * ADC_LSB2VOLT, 6;
      }
    }

    //for serial plotter
    for (int i = 0; i < ( sizeof(readings[i]) / sizeof(readings[0][0]) ); i++)
    {
      for (int j = 0; j < NUM_ADC; j++)
      {
        //comma- or space- delimited values show up as separate
        //traces in the IDE stripchart
        Serial.print(readings[i][j]);
        Serial.print(" ");
      }

      Serial.println();
    }

    delay(1);
  }

  //  sanity check
  //  for (;;)
  //  {
  //    Serial.println(analogRead(ADC_PIN_0)* ADC_LSB2VOLT);
  //    delay(100);
  //  }
}

